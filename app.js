var express = require("express");
let ejs = require("ejs");
var bodyParser = require("body-parser"); //passer les contenu des input
const querystring = require("querystring");
var jsonfile = require("jsonfile");
const fs = require("fs");

var urlencodedParser = bodyParser.urlencoded({ extended: false });

var app = express();
app.set("views", "./views");
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/"));
app.get("/", function(req, res) {
  res.render("index", { page: "index" });
});
app.get("/");
app.get("/:page", function(req, res) {
  if (
    req.params.page === "presentation" ||
    req.params.page === "louis_ix" ||
    req.params.page === "philippe_auguste" ||
    req.params.page === "hugues_capet" ||
    req.params.page === "philippe_le_bel" ||
    req.params.page === "adheration"
  ) {
    res.render(req.params.page, { page: req.params.page });
  } else {
    res.render("index");
  }
});
app.post("/bienvenueAdherant/", urlencodedParser, function(req, res) {
  let adherant = {
    nom: req.body.nom,
    prenom: req.body.prenom,
    naissance: req.body.naissance,
    adresse: req.body.adresse,
    code_postal: req.body.pstal,
    ville: req.body.ville,
    amail: req.body.email,
    tel: req.body.tel
  }; /*let aherant= req.body*/
  let donnees = JSON.stringify(adherant);

  fs.appendFile("fichier.json", donnees + "\n", function(err) {});
  let query = querystring.stringify({
    nom: req.body.nom,
    prenom: req.body.prenom,
    page: "adheration"
  });
  res.redirect("/" + query);
});
app.listen(8067);
